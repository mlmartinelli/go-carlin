package main

import (
	"fmt"
	"log"

	"github.com/SlyMarbo/rss"
)

var rssConfig = map[string]string{
	"clarin":  "https://www.clarin.com/rss/lo-ultimo/",
	"nacion":  "http://contenidos.lanacion.com.ar/herramientas/rss-origen=2",
	"pagina":  "https://www.pagina12.com.ar/rss/portada",
	"infobae": "https://www.infobae.com/argentina-rss.xml",
	"perfil":  "http://www.perfil.com/rss/ultimo-momento",
}

func SendRss(from int, key string) {
	val := rssConfig[key]
	if val == "" {
		val = key
	}

	feed, err := rss.Fetch(val)
	if err != nil {
		log.Println("Error fetching rss ", val, err)
		return
	}

	out := ""
	for i := 0; i < len(feed.Items) && i < 15; i++ {
		out = fmt.Sprintf("%s%s %s\n", out, feed.Items[i].Link, feed.Items[i].Title)
	}
	SendMessage(out, from, "")
}
