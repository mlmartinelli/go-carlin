package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
)

const COIN_URL = "https://api.coinmarketcap.com/v1/ticker/?limit=10"

type Coin struct {
	Symbol             string
	Price_usd          string
	Percent_change_1h  string
	Percent_change_24h string
	Percent_change_7d  string
}

func SendCoins(from int) {
	c, err := coins()
	if err != nil {
		return
	}

	lines := make([]string, len(c)+1)
	lines[0] = fmt.Sprintf("%-6s %10s - %6s%% - %6s%% - %6s%%",
		"SYMBOL", "U$D", "1h", "1d", "1w")
	for i := 0; i < len(c); i++ {
		price, _ := strconv.ParseFloat(c[i].Price_usd, 64)
		sprice := fmt.Sprintf("%8.5f", price)
		lines[i+1] = fmt.Sprintf("%-6s %10s - %6s%% - %6s%% - %6s%%",
			c[i].Symbol, sprice, formatPerc(c[i].Percent_change_1h), formatPerc(c[i].Percent_change_24h), formatPerc(c[i].Percent_change_7d))
	}
	message := fmt.Sprintf("<pre>%s</pre>", strings.Join(lines, "\n"))
	SendMessage(message, from, "HTML")
}

func formatPerc(perc string) string {
	if len(perc) > 0 && perc[0] != '-' {
		perc = fmt.Sprintf("+%s", perc)
	}
	return perc
}

func coins() ([]Coin, error) {
	resp, err := http.Get(COIN_URL)
	if err != nil {
		return nil, err
	}

	defer func() {
		err := resp.Body.Close()
		if err != nil {
			log.Println("Error closing coinmarketcap response body ", err)
			return
		}
	}()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var dat []Coin
	if err = json.Unmarshal(body, &dat); err != nil {
		return nil, err
	}
	return dat, nil
}
