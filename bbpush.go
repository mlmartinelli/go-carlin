package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
	"time"
)

type PushChange struct {
	New struct {
		Name   string
		Target struct {
			Links struct {
				Html struct {
					Href string
				}
			}
			Summary struct {
				Raw string
			}
		}
	}
}

type PushObj struct {
	Push struct {
		Changes []PushChange
	}
	Repository struct {
		Name  string
		Links struct {
			Html struct {
				Href string
			}
			Avatar struct {
				Href string
			}
		}
	}
	Actor struct {
		Username     string
		Display_name string
		Links        struct {
			Avatar struct {
				Href string
			}
		}
	}
}

func Bbhook(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" || r.Header.Get("Content-Type") != "application/json" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprint(w, "Method or content type not allowed.\n")
		return
	}

	thaTime := time.Now().UTC()
	hour := thaTime.Hour()
	week := thaTime.Weekday()
	if hour < 12 || hour > 20 || week == 0 || week == 6 {
		nano := thaTime.Nanosecond() % 4
		switch nano {
		case 0:
			SendMessage("https://www.youtube.com/watch?v=PCPtJzv5Y04", Conf.Telegram.Bbhookgroup, "")
		case 1:
			SendMessage("https://www.youtube.com/watch?v=IHrzBYf-5Y0&t=102s", Conf.Telegram.Bbhookgroup, "")
		case 2:
			SendMeme(Conf.Telegram.Bbhookgroup, "casa tomatela")
		case 3:
			SendMeme(Conf.Telegram.Bbhookgroup, "casa andate")
		}
	}

	err, msg := ParseBbHook(r.Body)
	if err != nil {
		log.Fatal("Error parsing bitbucket webhook", err)
	}

	SendMessage(msg, Conf.Telegram.Bbhookgroup, "")
}

func ParseBbHook(body io.Reader) (error, string) {
	var t PushObj
	decoder := json.NewDecoder(body)
	err := decoder.Decode(&t)
	if err != nil {
		return err, ""
	}
	lines := make([]string, len(t.Push.Changes))
	for i := 0; i < len(t.Push.Changes); i++ {
		lines[i] = fmt.Sprintf("@%s %s\n%s",
			t.Push.Changes[i].New.Name,
			strings.TrimSpace(t.Push.Changes[i].New.Target.Summary.Raw),
			t.Push.Changes[i].New.Target.Links.Html.Href)
	}
	changes := strings.Join(lines, "\n====\n")
	separator := "--------"

	actor := fmt.Sprintf("%s",
		t.Actor.Display_name)

	out := fmt.Sprintf("%s\n%s\n", actor, changes)
	return nil, out
}
